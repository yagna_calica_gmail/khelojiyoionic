import { StopWatch } from "./../pages/stopwatch/stopwatch";
import { User } from "./../model/user.model";
import { LoginPage } from "../pages/login/login";
import { FirebaseProvider } from "../providers/firebase/firebase";
import { TeamProfile } from "../pages/teamprofile/teamprofile";
import { Component, ViewChild } from "@angular/core";
import { Nav, Platform, Events, MenuController } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { MyTeam } from "../pages/myteam/myteam";
import { UserProfile } from "../pages/userprofile/userprofile";
import { Observable } from "rxjs/Observable";
import * as firebase from "firebase";
import { AppPreferences } from "@ionic-native/app-preferences";
import {
  Url,
  DataServiceProvider
} from "../providers/data-service/data-service";
import { AddEvent } from "../pages/addevent/addevent";
import { Storage } from "@ionic/storage";
import { Rosters } from "../pages/roster/roster";
import { PlayerProfile } from "../pages/playerprofile/playerprofile";

@Component({
  templateUrl: "app.html"
})
export class KheloJiyo {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  SuperUser: firebase.User;
  pages: Array<{ title: string; component: any }>;
  uber: Observable<User[]>;
  userImage = "assets/imgs/avatar.jpeg";
  firstName = this.globVar.user ? this.globVar.user.firstName : "";
  lastName = this.globVar.user ? this.globVar.user.lastName : "";
  email = this.globVar.user ? this.globVar.user.email : "";

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    public api: DataServiceProvider,
    splashScreen: SplashScreen,
    public firebaseProvider: FirebaseProvider,
    public globVar: Url,
    private storage: Storage,
    public publishEvent: Events,
    public menuCtrl: MenuController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      setTimeout(() => {
        splashScreen.hide();
        this.viewDidLoad();
      }, 800);
      statusBar.styleDefault();
    });
    this.subscribeEvents();
    // Left Drwaer list is intiallized here
    this.pages = [
      { title: "Home", component: TeamProfile },
      { title: "My Teams", component: MyTeam },
      { title: "Rosters", component: Rosters },
     // { title: "Stopwatch", component: StopWatch }
    ];
  }

  subscribeEvents() {
    if (!this.globVar.appSubcribeflag) {
      this.globVar.appSubcribeflag = true;
      this.publishEvent.subscribe("userProfile", data => {
        if (data.action == "loadUserProfile") {
          this.refreshProfile();
        } else if (data.action == "controlMenuDisable") {
          this.menuCtrl.enable(false, "leftMenu");
        } else if (data.action == "controlMenuenable") {
          this.menuCtrl.enable(true, "leftMenu");
        }
      });
    }
  }

  viewDidLoad() {
    try {
      this.storage.get("token").then(
        res => {
          if (res !== null && res !== "") {
            this.globVar.token = res;
            this.storage.get("user").then(
              (usr: User) => {
                if (usr !== null) {
                  this.globVar.user = usr;
                  this.globVar.isPlayer = (usr.roleId == 1)?false:true;
                  this.updatemenu();
                  this.userImage = "assets/imgs/avatar.jpeg";
                  this.firstName = this.globVar.user
                    ? this.globVar.user.firstName
                    : "";
                  this.lastName = this.globVar.user
                    ? this.globVar.user.lastName
                    : "";
                  this.email = this.globVar.user ? this.globVar.user.email : "";
                  this.nav.setRoot(TeamProfile);
                } else {
                  this.nav.setRoot(LoginPage);
                }
              },
              error => {
                alert("Error.message" + error);
              }
            );
          } else {
            this.nav.setRoot(LoginPage);
          }
        },
        error => {
          alert("Error.message" + error);
          this.nav.setRoot(AddEvent);
          this.nav.setRoot(LoginPage);
        }
      );
    } catch (Error) {
      alert(Error.message);
    }
  }

  updatemenu(){
    if(this.globVar.isPlayer){
      this.pages = [
        { title: "Home", component: TeamProfile },
        { title: "My Teams", component: MyTeam },
       // { title: "Rosters", component: Rosters },
        { title: "Stopwatch", component: StopWatch }
      ];
    }
    else
   { this.pages = [
      { title: "Home", component: TeamProfile },
      { title: "My Teams", component: MyTeam },
      { title: "Rosters", component: Rosters },
      { title: "Stopwatch", component: StopWatch }
    ];
  }

  }

  refreshProfile() {
    try {
      this.storage.get("token").then(
        res => {
          if (res != null && res != "") {
            this.globVar.token = res;
            this.storage.get("user").then(
              (usr: User) => {
                if (usr !== null) {
                  this.globVar.user = usr;
                  this.globVar.isPlayer = (usr.roleId == 1)?false:true;
                  this.updatemenu();
                  this.userImage = "assets/imgs/avatar.jpeg";
                  this.firstName = this.globVar.user
                    ? this.globVar.user.firstName
                    : "";
                  this.lastName = this.globVar.user
                    ? this.globVar.user.lastName
                    : "";
                  this.email = this.globVar.user ? this.globVar.user.email : "";
                } else {
                  this.nav.setRoot(LoginPage);
                }
              },
              error => {
                alert("Error.message" + error);
              }
            );
          } else {
            this.nav.setRoot(LoginPage);
          }
        },
        error => {
          alert("Error.message" + error);
          this.nav.setRoot(AddEvent);
          this.nav.setRoot(LoginPage);
        }
      );
    } catch (Error) {
      alert(Error.message);
    }
  }

  moveNext() {

  }

  openPage(page) {
    if (page.component == this.nav.getActive().component) {
      this.api.showToast("Already Open");
      return;
    }
    this.nav.setRoot(page.component);
  }

  userProfile() {
    if(this.globVar.isPlayer){
      this.nav.push(PlayerProfile);
    }
    else{
      this.nav.push(UserProfile);

    }
  }

  logout() {
    this.nav.setRoot(LoginPage);
    this.storage.remove("token").then(() => {
      this.storage.remove("user").then(() => {
        this.globVar.userImage = "assets/imgs/avatar.jpeg";
        this.globVar.selectedTeamIndex = 0;
        this.globVar.teams = [];
        this.globVar.firstName = "";
        this.globVar.lastName = "";
        this.globVar.email = "";
        this.globVar.token = "";
        this.globVar.isPlayer=false;
        this.nav.setRoot(LoginPage);
      });
    });
  }
}
