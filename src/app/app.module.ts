import { StopWatch } from './../pages/stopwatch/stopwatch';
import { Roster } from './../model/rosters.model';
import { TeamProfileTab } from "./../pages/teamprofiletab/teamprofiletab";
// import { HttpClient } from '@angular/common/http';
import { TeamProfile } from "../pages/teamprofile/teamprofile";
import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { KheloJiyo } from "./app.component";
import { HomePage } from "../pages/home/home";
import { AboutPage } from "../pages/about/about";
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/Register/register";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { Geolocation } from "@ionic-native/geolocation";
import { Device } from "@ionic-native/device";
import { Facebook } from "@ionic-native/facebook";
import {
  DataServiceProvider,
  Url
} from "../providers/data-service/data-service";
import { SpeechRecognition } from "@ionic-native/speech-recognition";
import { TextToSpeech } from "@ionic-native/text-to-speech";
import { Camera } from "@ionic-native/camera";
import { PhotoLibrary } from "@ionic-native/photo-library";
import { KeysPipe } from "../pipes/keys.pipe";
import { MyTeam } from "../pages/myteam/myteam";
// Import the AF2 Module
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from "angularfire2/database";
import { FirebaseProvider } from "../providers/firebase/firebase";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuth } from "angularfire2/auth";
import { ForgotPage } from "../pages/forgot/forgot";
import { GooglePlus } from "@ionic-native/google-plus";
import { SplashScreen } from "@ionic-native/splash-screen";
import { AppPreferences } from "@ionic-native/app-preferences";
import { UserProfile } from "../pages/userprofile/userprofile";
import { AddRoster } from "../pages/rosterstab/addroster";
import { RostersTab } from "../pages/rosterstab/rosterstab";
import { Rosters } from "../pages/roster/roster";
import { AddCoach } from "../pages/teamprofiletab/addcoach";
import { ScheduleTab } from "../pages/scheduletab/scheduletab";
import { SelectRole } from "../pages/login/selectrole";
import { AddTeam } from "../pages/addteam/addteam";
import { AddEvent } from "../pages/addevent/addevent";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { IonicStorageModule } from "@ionic/storage";
import { Race100M } from "../pages/score/100m/100mrace";
import { from } from 'rxjs';
import { TranslateModule } from 'ng2-translate';
import {ScrollingHeaderModule} from 'ionic-scrolling-header';
import { AddExp } from "../pages/userprofile/addExp/addExp";
import { PlayerProfile } from '../pages/playerprofile/playerprofile';
import { PersonalTab } from '../pages/playerprofile/personaltab/personaltab';
import { Acadmicstab } from '../pages/playerprofile/acadmicstab/acadmicstab';
import { Athleticstab } from '../pages/playerprofile/athleticstab/athleticstab';
import { AddEducation } from '../pages/playerprofile/acadmicstab/addEducation/addEducation';
import { Stat100M } from '../pages/stats/stat100m/stat100m';


//import { AlertController } from 'ionic-angular';
// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyD6QoZtgVsMJnOlV2-5BS7R_OSJyTIeKr8",
  authDomain: "khelojiyoionic.firebaseapp.com",
  databaseURL: "https://khelojiyoionic.firebaseio.com",
  projectId: "khelojiyoionic",
  storageBucket: "khelojiyoionic.appspot.com",
  messagingSenderId: "899015697986"
};

@NgModule({
  declarations: [
    KheloJiyo,
    HomePage,
    MyTeam,
    AboutPage,
    TeamProfile,
    LoginPage,
    RegisterPage,
    Race100M,
    KeysPipe,
    Rosters,
    ForgotPage,
    UserProfile,
    TeamProfileTab,
    AddRoster,
    Rosters,
    RostersTab,
    AddCoach,
    ScheduleTab,
    SelectRole,
    AddTeam,
    AddEvent,
    StopWatch,
    AddExp,PlayerProfile,PersonalTab,Athleticstab,Acadmicstab,AddEducation,
    Stat100M

  ],
  imports: [
    BrowserModule, TranslateModule.forRoot(),
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(KheloJiyo),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    IonicStorageModule.forRoot(),
    ScrollingHeaderModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    KheloJiyo,
    HomePage,
    MyTeam,
    AboutPage,
    TeamProfile,
    LoginPage,
    RegisterPage,
    RostersTab,
    Rosters,
    UserProfile,
    TeamProfileTab,
    TeamProfileTab,
    Race100M,
    ForgotPage,
    AddRoster,
    AddCoach,
    ScheduleTab,
    SelectRole,
    AddTeam,
    AddEvent,
    StopWatch,
    AddExp,
    PlayerProfile,PersonalTab,Athleticstab,Acadmicstab,AddEducation,
    Stat100M

  ],
  providers: [
    StatusBar,TranslateModule,
    SplashScreen,
    AboutPage,
    TeamProfile,
    LoginPage,
    RegisterPage,
    GooglePlus,
    DataServiceProvider,
    SpeechRecognition,
    TextToSpeech,
    Race100M,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Geolocation,
    Device,
    Facebook,
    Rosters,
    RostersTab,
    StopWatch,
    Camera,
    PhotoLibrary,
    ForgotPage,
    UserProfile,
    TeamProfileTab,
    FirebaseProvider,
    Url,
    AppPreferences,
    AddRoster,
    AddCoach,
    ScheduleTab,
    SelectRole,
    AddTeam,
    AddEvent,
    AngularFireAuth,
    DatePicker,
    AddExp,PlayerProfile,PersonalTab,Athleticstab,Acadmicstab,AddEducation,
    Stat100M


  ]
})
export class AppModule {}
