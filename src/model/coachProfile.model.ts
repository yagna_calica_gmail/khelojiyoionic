export class coachProfile {
id:String;
aspnetuserId:String;
gender:String;
contactNo:String;
dob:Date;
country:String;
state:String;
city:String;
pinCode:String;
instituteName:String;
instituteAddress:String;
experience:string;
achievements:String;
coachingPhilosophy:String;
isDeleted:String;

// easpnetuserId:String="N/A";
// egender:String="N/A";
// econtactNo:String="N/A";
// edob:String="N/A";
// ecountry:String="N/A";
// estate:String="N/A";
// ecity:String="N/A";
// epinCode:String="N/A";
// einstituteName:String="N/A";
// einstituteAddress:String="N/A";
// eexperience:String="N/A";
// eachievements:String="N/A";
// ecoachingPhilosophy:String="N/A";
// eisDeleted:String="N/A";
}
