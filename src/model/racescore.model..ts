export class RaceScore {
  eventName:String;
  raceTime:number  = 0.00;
  rank:number= 0;
  playerName:String;
  playerId:String;
  achievement: String;
  averageTimeScore:String;
  bestTimeScore:String;
  kms: String;
  longestMarathon:String;
  raceType:String;
  totalRaceLost: String;
  totalRaceParticipated: String;
  totalRaceWon:String;
  winRatio: String;
  eRank: number = 0;
  eRaceTime: number =0.00;
  isChecked: boolean = false;
  isEdit:boolean=false;



}


