import { Error } from "./error.model";
import { User } from "./user.model";

export class APIResponse {
    status: string;
    errors: Error[];
    data: string;

    constructor(values: Object = {}) {

        Object.assign(this, values);

    }
}



