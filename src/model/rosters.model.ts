export class Roster {
    id:String;
    name:String;
    jerseyNumber:String;
    emailId:String;
    isEdit:boolean=false;
    Rank: String = "0";
    RaceTime: String = "0.00";
    eRank: String = "0";
    eRaceTime: String ="0.00";
    userId:string;
    isChecked: boolean = false;
}
