
export class Stat100m {
  id:string;
  aspnetuserId:string;
  totalRacesWon:string;
  totalRaceLost:string;
  totalRacesParticipated:string;
  winRatio:string;
  bestScore:string;
  worstScore:string;
  achievements:string;
  isDeleted:string;
}
