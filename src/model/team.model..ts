export class Team {

    id: String;
    name: String;
    teamType: String;
    profileImage: String;
    teamImage: String;
    createdByUserId: 0;
    aspnetuserId: String;
    sportsId: string;
    createdDate: String;
    modifiedDate: String;
    isDeleted: String;
    createdByUser: String;
    sports: String;
    cricketTeamInnings: String;
    organizationTeams: String;
    scoreCricketFinal: String;
    scoreHockeyFinal: String;
    scoreSoccerFinal: String;
    scoreboardCricketBatting: String;
    scoreboardCricketBowling: String;
    scoreboardCricketExtras: String;
    scoreboardCricketWicketsBatsmanTeam: String;
    scoreboardCricketWicketsBowlerTeam: String;
    scoreboardHockey: String;
    scoreboardSoccer: String;
    scoreboardTennisTeamA: String;
    scoreboardTennisTeamB: String;
    teamEvents: String;
    tennisEventUsers: String;
    tennisScoreFinal: String;
    userTeam: String;
}
