export class User {
  firstName: String;
  lastName: String;
  roleId: number;
  id: String;
  userName: String;
  normalizedUserName: String;
  email: String;
  normalizedEmail: String;
  emailConfirmed: boolean;
  passwordHash: String;
  securityStamp: String;
  concurrencyStamp: String;
  phoneNumber: String;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
  accessFailedCount: String;
  userImage: String;
  aspnetuserclaims: Array<any>;
  aspnetuserlogins: Array<any>;
  aspnetuserroles: Array<any>;
  aspnetusertokens: Array<any>;
  lockoutEnabled: boolean;
  lockoutEnd: String;
}
