import { Error } from './../../model/error.model';
import { User } from "../../model/user.model";
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import { Component } from "@angular/core";
import { FirebaseProvider } from "../../providers/firebase/firebase";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";


import { Observable } from "rxjs/Observable";

@Component({
  selector: "page-register",
  templateUrl: "register.html"
})
@Injectable()
export class RegisterPage {
  public Firstname: string = "";
  public Lastname: string = "";
  public isCoach: boolean = true;
  public Password: string = "";
  public Password_2: string = "";
  public Email: string = "";
  public Phone: string = "";
  public u_email: string = "";
  public flag: boolean = false;
  public pre_user: firebase.User;
  public uber: Observable<User[]>;
  public m_user: User[];
  public loading;

  signupError: string;
  constructor(
    public navParams: NavParams,
    private navCtrl: NavController,
    public http: Http,
    public firebaseProvider: FirebaseProvider,
    public api: DataServiceProvider,
    public url: Url,
    public loadingCtrl: LoadingController
  ) {
  }

  signup(): void {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });

    if(!this.valid())
      { return;
      }
    this.loading.present();
    let obj = {
      FirstName: this.Firstname,
      LastName: this.Lastname,
      Email: this.Email.toLowerCase(),
      Password: this.Password,
      RoleId: this.isCoach ? "1" : "2",
      PhoneNumber: this.Phone
    };

    this.api.postRequset(this.url.register, obj).subscribe(
      response_api => {
        this.loading.dismiss();
        if (response_api.status == Constants.OK) {
          this.api.showToast("You have registered successfully.");
          this.finish();
        } else {
          this.api.showToast(
            "Registration Failed: " + response_api.errors[0].code
          );
        }
      },
      error => {
        this.loading.dismiss();
        this.api.showToast("Something Went wrong");
      }
    );
  }

  selectcoach(value: boolean) {
    this.isCoach = value;
  }

  valid() {
    if (this.Firstname.length == 0) return false;
    if (this.Lastname.length == 0) return false;
    if (this.Email.length == 0) return false;
    if (this.Phone.length == 0) return false;
    if (this.Password.length == 0) return false;
    if (this.Password_2.length == 0) return false;
    if (this.Password != this.Password_2) {
      this.api.showToast("Passwords does not match!");
      return false;
    }
    else return true;
  }

  finish() {
    this.navCtrl.pop();
  }
}
