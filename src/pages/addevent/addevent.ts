import { Component, ViewChild } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Platform,
  Keyboard
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../model/rosters.model";
import { Renderer } from "@angular/core";
import * as Constants from "../../model/constant.model";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { ToastController } from "ionic-angular";

@Component({
  selector: "page-addevent",
  templateUrl: "addevent.html"
})
export class AddEvent {
  @ViewChild("navbar") navBar: Navbar;
  public loading;
  public sports: any[] = Constants.sports;
  teamImage = "assets/imgs/avatar.jpeg";
  sportName = this.sports[this.globVar.team ? this.globVar.team.sportsId : "1"];
  eventDate: Date;
  eventType = "Tournament";
  gameType = "Game";
  venue = "";
  public countries: Array<any> = [];
  venueId = "";
  public rosters: Array<Roster> = [];
  private list: Array<any> = [];
  isNoPlayer: boolean = false;
  title= "";
  constructor(
    platform: Platform,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private navCtrl: NavController,
    private datePicker: DatePicker,
    private keyboard: Keyboard
  ) {
    console.log("UserId", params.get("userId"));
  }
  ionViewDidLoad(): void {
    this.loadTeamRosters();
  }
  loadTeamRosters() {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    this.api
      .getRequset(
        this.url.getTeamRosters(this.globVar.team.id, this.globVar.user.id)
      )
      .subscribe(
        response_api => {
          try {
            this.loading.dismiss();
            this.rosters = response_api.data.athleteList;
            if (this.rosters.length > 0) {
              this.isNoPlayer = false;
            } else {
              this.isNoPlayer = true;
              this.rosters = [];
              this.api.showToast("Please, Add the atheltes to the Team");
            }
            // alert("teams"+response_api.data.teamList.length );
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
          this.loading.dismiss();
          this.api.showToast("Error: Request failed-" + error.message);
        }
      );
  }

  discard() {
    this.viewCtrl.dismiss();
  }


  save() {
     if(this.venueId == ""){
      this.api.showToast("Please, provide the venue!");
      return;
    }
    else if(!this.eventDate)
    {
      this.api.showToast("Please, provide the Date & Time!");
      return;
    }

    else if (this.isNoPlayer) {
      this.api.showToast("No Athletes in the Team!");
      return;
    }
     else {
      let flag = false;
      for (var i = 0; i < this.rosters.length; i++) {
        if (this.rosters[i].isChecked) {
          flag = true;
          break;
        }
      }
      if (!flag) {
        this.api.showToast("Please select athlete first!");
        return;
      }
    }

    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let eventName = this.title + ", ";
    //let eventName = this.sports[this.globVar.team.sportsId] + " Race, ";
    if (this.gameType == "Game" && this.eventType == "Tournament") {
      eventName = eventName + "  Tournament";
    } else if (this.gameType == "Game" && this.eventType == "Exhibition") {
      eventName = eventName + " Exhibition";
    } else if (this.gameType == "Practise") {
      eventName = eventName + " Practise";
    }

   let participant : Array<String> =[];
   let index=0;
   for (var ii = 0; ii < this.rosters.length; ii++) {
    if (this.rosters[ii].isChecked)
     {
       participant[index]=this.rosters[ii].userId;
       index++;
     }
   }

    let obj = {
      EventName: eventName,
      AspnetuserId: this.globVar.user.id,
      Venue: this.venueId,
      ScheduledDate: this.eventDate,
      SportsType: this.globVar.team.sportsId,
      Participant : participant
    };

    this.api
      .postRequset(this.url.createEvent + this.globVar.team.id, obj)
      .subscribe(
        response_api => {
          // var response_api  = new APIResponse(response);
          //alert("successiotyufg"+ response_api.status);
          this.loading.dismiss();
          try {
            let data=   {
              action: "Done"
            };
            if (response_api.status == Constants.OK) {
              this.viewCtrl.dismiss(data);
            } else {
              this.api.showToast("Failed");
            }
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
          this.loading.dismiss();
          this.api.showToast("Error: Request failed-" + error.message);
        }
      );
  }

  datePickerfunc() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => console.log("Got date: ", date),
        err => console.log("Error occurred while getting date: ", err)
      );
  }

  add(item: any) {
    this.countries = [];
    this.countries.length = 0;
    this.venueId = item.id;
    this.venue = item.placeName;
  }

  removeFocus() {
    this.keyboard.close();
  }

  search() {
    if (this.venue.trim().length == 0) {
      this.countries = [];
      return;
    }

    this.api
      .getRequset(this.url.getVenueList + this.venue)
      .subscribe(response_api => {
        try {
          this.list = response_api.data.venueList;
          // if (!this.emailId.trim().length || !this.keyboard.isOpen()) {
          if (!this.venue.trim().length) {
            this.countries = [];
            return;
          }
          this.countries = this.list.filter(item =>
            item.placeName.toUpperCase().includes(this.venue.toUpperCase())
          );
        } catch (Error) {
          console.log("" + Error);
        }
      });
  }

  selectAll() {
    for (var i = 0; i < this.rosters.length; i++) {
      this.rosters[i].isChecked = true;
    }
  }
  deSelectAll() {
    for (var i = 0; i < this.rosters.length; i++) {
      this.rosters[i].isChecked = false;
    }
  }
}
