import { Component, ViewChild } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Platform
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../model/rosters.model";
import { Renderer } from "@angular/core";
import * as Constants from "../../model/constant.model";

@Component({
  selector: "page-addteam",
  templateUrl: "addteam.html"
})
export class AddTeam {

  @ViewChild('navbar') navBar: Navbar;
  public loading;
  teamImage =  "assets/imgs/avatar.jpeg";
  sport: String = "1";
  gender: String ="M";
  teamName : String = "";
   teamId: number = 0;

  sports : any[] = Constants.sports_arr;
  constructor(
    platform: Platform,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private navCtrl: NavController,

  ) {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    console.log("UserId", params.get("userId"));
  }

discard(){
  this.viewCtrl.dismiss()
}
save(){
  this.loading.present();
  let obj = {
     Name:this.teamName,
     TeamType:this.gender,
     SportsId:(this.teamId+1),
     AspnetuserId:this.globVar.user.id,
     CreatedByUserId:"1",
     IsDeleted: "0"
  };


  this.api.postRequset(this.url.addTeam, obj).subscribe(
    response_api => {
      // var response_api  = new APIResponse(response);
      //alert("successiotyufg"+ response_api.status);
      this.loading.dismiss();
      try {
        if (response_api.status == Constants.OK) {
          this.viewCtrl.dismiss();
        } else {
          alert("Failed: " + response_api.status);
        }
      } catch (Error) {
        alert(Error.message);
      }
    },
    error => {
      this.loading.dismiss();
      alert("Error: Request failed-" + error.message);
    }
  );
}

}
