import { KheloJiyo } from "./../../app/app.component";
import { TeamProfile } from "./../teamprofile/teamprofile";
import { User } from "./../../model/user.model";
import { Observable } from "rxjs/Observable";
import { RegisterPage } from "../Register/register";
import { Component, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import { AngularFireDatabase } from "angularfire2/database";
import { FirebaseProvider } from "../../providers/firebase/firebase";
import {
  NavController,
  LoadingController,
  ModalController,
  Events
} from "ionic-angular";
import { ForgotPage } from "../forgot/forgot";
import { GooglePlus } from "@ionic-native/google-plus";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from "@ionic-native/app-preferences";
import { SelectRole } from "./selectrole";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  @ViewChild("email") email: any;
  public username: string;
  public password: string;
  public error: string;
  passwordType: string = "password";
  paIcon: string = "eye-off";
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  public loading;

  constructor(
    private firebaseProvider: FirebaseProvider,
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public afDatabase: AngularFireDatabase,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public facebook: Facebook,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    private storage: Storage,
    public publishEvent: Events
  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
    setTimeout(() => {
      this.publishEvent.publish("userProfile",{action:"controlMenuDisable"} );
      this.email.setFocus();
    }, 500);
  }

  login(): void {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let obj = {
      email: this.username,
      password: this.password
    };
    this.api.postRequset(this.url.login, obj).subscribe(
      response_api => {
        // var response_api  = new APIResponse(response);
        //alert("successiotyufg"+ response_api.status);
        this.loading.dismiss();
        try {
          if (response_api.status == Constants.OK) {
            this.loadDashboard(
              response_api.data.appuser,
              response_api.data.token
            );
          } else {
            this.api.showToast("Failed");
          }
        } catch (Error) {
          this.api.showToast("Failed");
        }
      },
      error => {
        this.loading.dismiss();
        this.api.showToast("failed");
      }
    );
  }

  loadDashboard(user: any, token: any) {
    this.publishEvent.publish("userProfile",{action:"controlMenuenable"} );
    this.globVar.user = user;
    this.globVar.token = token;
    this.globVar.firstName = this.globVar.user.firstName;
    this.globVar.lastName = this.globVar.user.lastName;
    this.globVar.email = this.globVar.user.email;
    this.storage.set("user", user);
    this.storage.set("token", token);
    if (token != "") {
      this.navCtrl.setRoot(TeamProfile);
    }
  }

  socialLogin(param: any, googleUser: any) {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    this.api.postRequset(this.url.socailLogin, param).subscribe(
      response_api => {
        this.loading.dismiss();
        try {
          if (
            response_api.status == Constants.OK &&
            response_api.data.isSocialUser
          ) {
            this.loadDashboard(response_api.data.appUser,  response_api.data.token
            );
          } else if (
            response_api.status == Constants.OK &&
            !response_api.data.isSocialUser
          ) {
            let selectRoleModal = this.modalCtrl.create(SelectRole, {
              param: googleUser
            });
            selectRoleModal.present();
          } else {
            this.api.showToast("Failed");
          }
        } catch (Error) {
          alert(Error.message);
        }
      },
      error => {
        this.loading.dismiss();
        alert("Error: Request failed-" + error.message);
      }
    );
  }

  signup() {
    this.navCtrl.push(RegisterPage, { email: "null" });
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.paIcon = this.paIcon === "eye-off" ? "eye" : "eye-off";
  }

  forgotPassword() {
    this.navCtrl.push(ForgotPage);
  }

  loginWithGoogle() {
    this.firebaseProvider.signInWithGoogle().then(
      user => {
        if (user.user.emailVerified) {
          user = user.user;
          let uName = user.displayName.split(" ");
          let googleObj = {
            Email: user.email,
            RoleId: "1",
            Password: "Qwer123#",
            FirstName: uName[0],
            LastName: uName[1],
            PropertyType: "profile_id",
            PropertyValue: user.email,
            SocialAccountType: "Google",
            image: user.photoURL
          };

          let chackObj = {
            Email: user.email,
            SocialAccountType: "Google"
          };

          this.socialLogin(chackObj, googleObj);
        }else {
          alert("Email not varifired.Please check your mailbox");
        }

        // if (user.emailVerified) {
        //   let uName = user.displayName.split(" ");
        //   alert(""+JSON.stringify(user));
        //   let googleObj = {
        //     Email: user.email,
        //     RoleId: "1",
        //     Password: "Qwer123#",
        //     FirstName:uName[0],
        //     LastName:uName[1],
        //     PropertyType: "profile_id",
        //     PropertyValue: user.email,
        //     SocialAccountType: "Google",
        //     image: user.photoURL
        //   };

        //   let chackObj = {
        //     Email: user.email,
        //     SocialAccountType: "Google"
        //   };

        //   this.socialLogin(chackObj, googleObj);
        // }
      },
      error => console.log(error.message)
    );
  }

  loginWithFacebook() {
    this.facebook
      .login(["public_profile", "user_photos", "email", "user_birthday"])
      .then((res: FacebookLoginResponse) => {
        if (res.status === "connected") {
          // this.isLoggedIn = true;
          this.facebook
            .api(
              "/" +
                res.authResponse.userID +
                "/?fields=id,email,name,picture,gender",
              ["public_profile"]
            )

            .then(user => {
              var name = user.name.split(" ");
              var email = user.email;
              var id = user.id;

              let fbobj = {
                Email: email,
                RoleId: "1",
                Password: "Qwer123#",
                FirstName: name[0],
                LastName: name.length > 1 ? name[1] : "",
                PropertyType: "profile_id",
                PropertyValue: id,
                SocialAccountType: "Facebook"
              };
              let chackObj = {
                Email: user.email,
                SocialAccountType: "Facebook"
              };
              this.socialLogin(chackObj, fbobj);
              //  alert("succes" + name+"-"+email+"-"+picture+"-"+id);
              // this.users = res;
            })
            .catch(e => {
              alert("exception" + e);
              console.log(e);
            });
        } else {
          alert("failed" + res);
          // this.isLoggedIn = false;
        }
      })
      .catch(Error => alert("Error logging into Facebook---" + Error.message));
  }

  loginWithGooglePlus() {
    // alert("succed");
    this.gPlus
      .login({
        scopes: "", // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
        webClientId: environment.googleWebClientId, // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
        offline: true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
      })
      .then(
        user => {
          //save user data on the native storage\
          // this.nativeStorage.setItem('google_user', {
          //   name: user.displayName,
          //   email: user.email,
          //   picture: user.imageUrl
          // }).then(() => {
          // }, (error) => {
          //   console.log(error);
          // })
        },
        err => {
          alert("failed" + err);
        }
      );
  }
}

// loginFirebase() {
//   debugger;
//   if (!this.username) {
//     return;
//   }
//   let credentials = {
//     email: this.username,
//     password: this.password
//   };
//   this.firebaseProvider.signInWithEmail(credentials).then(
//     user => {
//       if (user.user.emailVerified) {
//         this.firebaseProvider.Superuser = user.user;
//         var pre_user = user.user;
//         if (pre_user) {
//           this.firebaseProvider.findEmail(pre_user.email).then(ref => {
//             this.uber = ref;
//             ////
//             this.uber.subscribe(
//               (res: User[]) => {
//                 console.log(res);
//                 var m_user = res;
//                 if (m_user.length === 0) {
//                   this.firebaseProvider.isRegistredUser = false;
//                   this.navCtrl.setRoot(RegisterPage, {
//                     email: pre_user.email
//                   });
//                 } else {
//                   //   this.firebaseProvider.username = res[0].Username;
//                   this.navCtrl.setRoot(HomePage);
//                 }
//               },
//               () => {
//                 this.firebaseProvider.isRegistredUser = false;
//                 return false;
//               }
//             );
//           });
//         }

//         ////////////////
//       } else {
//         alert("Email not varifired.Please check your mailbox");
//         this.firebaseProvider.sendEmailVerificationLink(user.user).then(
//           ref => {},
//           error => {
//             alert(error.message);
//           }
//         );
//       }
//     },
//     error => {
//       alert(error.message);
//       this.loginError = error.message;
//     }
//   );
// }

// async loginWithGooglePlus() {
//   // alert("succed");
//   this.gPlus
//     .login({
//       scopes: "", // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
//       webClientId: environment.googleWebClientId, // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
//       offline: true // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
//     })
//     .then(
//       user => {
//         //save user data on the native storage\
//         alert("succed");
//         // this.nativeStorage.setItem('google_user', {
//         //   name: user.displayName,
//         //   email: user.email,
//         //   picture: user.imageUrl
//         // }).then(() => {

//         // }, (error) => {
//         //   console.log(error);
//         // })
//       },
//       err => {
//         alert("failed" + err);
//       }
//     );
// }

// Function for Sigin in With Google using FirebaseAuth

// saveData() {
//   const obj = {
//     UserName: "N/A",
//     Password: "N/A",
//     Name: "N/A",
//     Email: this.firebaseProvider.getEmail()
//   };
//   this.firebaseProvider.addUser(obj);
// }
