import { TeamProfile } from "../teamprofile/teamprofile";
import { User } from "../../model/user.model";
import { Observable } from "rxjs/Observable";
import { RegisterPage } from "../Register/register";
import { Component, ViewChild } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AngularFireDatabase } from "angularfire2/database";
import { FirebaseProvider } from "../../providers/firebase/firebase";
import { NavController, LoadingController, NavParams, ModalController, ViewController } from "ionic-angular";
import { HomePage } from "../home/home";
import { ForgotPage } from "../forgot/forgot";
import * as firebase from "firebase";
import { firebaseConfig } from "../../app/app.module";
import { GooglePlus } from "@ionic-native/google-plus";
import { environment } from "../../environments/environment";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { Facebook } from "@ionic-native/facebook";
import { APIResponse } from "../../model/response.model";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from "@ionic-native/app-preferences";

 @Component({
    selector: "page-selectrole",
    templateUrl: "selectrole.html"
  })
export class SelectRole {
  public loading;
  public obj;

 role : String ="Coach";
 Coach : String ="Coach";
 Player : String ="Player";
 constructor(params: NavParams,
  public api: DataServiceProvider,
  public url: Url,
  public loadingCtrl: LoadingController,
  public globVar: Url,
  public prefs: AppPreferences,
  private navCtrl: NavController,
  public viewCtrl: ViewController,

  )
 {

  this.loading = this.loadingCtrl.create({ content: "please wait..." });

   this.obj= params.get('param');
 }

socialSignup(){
  this.loading.present();
  this.obj.RoleId=this.role;


  this.api.postRequset(this.url.socialRegistration, this.obj).subscribe(
    response_api => {
      this.loading.dismiss();
      try {
        if (response_api.status == Constants.OK) {
          this.globVar.user = response_api.user;
          this.globVar.token = response_api.jwt;
          this.prefs.store("user", response_api.user);
          this.prefs.store("token", this.globVar.token);
          if (this.globVar.token != "") {
           this.navCtrl.setRoot(TeamProfile);
          }
        } else {
          alert("Failed"+JSON.stringify(response_api));
        }
      } catch (Error) {
        alert(Error.message);
      }
    },
    error => {
      this.loading.dismiss();
      alert("Error: Request failed-" + error.message);
    }
  );
  }

 selection(role: string) {
  this.role= role;
 }


}

