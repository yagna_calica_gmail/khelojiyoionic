import { TeamProfile } from './../teamprofile/teamprofile';
import { TeamProfileTab } from './../teamprofiletab/teamprofiletab';
import { AddTeam } from './../addteam/addteam';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import { sports } from './../../model/constant.model';
import { User } from "../../model/user.model";
import { Observable } from "rxjs/Observable";
import { Component, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import {Content, NavController, LoadingController,Navbar, ModalController, Events } from "ionic-angular";
import { GooglePlus } from "@ionic-native/google-plus";
import {  Url,  DataServiceProvider } from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from '@ionic-native/app-preferences';
import { AssistantCoach } from "../../model/AssistantCoach.model";
import { Team } from '../../model/team.model.';


@Component({
  selector: 'page-myteam',
  templateUrl: 'myteam.html'
})

export class MyTeam {
  @ViewChild('navbar') navBar: Navbar;
  public username: string;
  public password: string;
  public error: string;
  public sports : any[] = Constants.sports;
  teams: Array<Team> = [];
  loginError: string;
  uber: Observable<any>;
  teamImage ="assets/imgs/team.png";

  @ViewChild(Content) _content: Content;
  addTeamCalled:boolean = false;

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public assistantCoach : Array<AssistantCoach>;
  public loading;

  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public publishEvent: Events


  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
  //  this.loadTeams();

  }


  ionViewDidEnter() {
  }





  loadTeams(): void {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
  //"0116ffc9-08ce-4b0f-a441-cde674103c58"
    this.api.getRequset(this.url.getTeamsbyId + this.globVar.user.id).subscribe(
      (response_api) => {
        this.loading.dismiss();
        try {
          this.globVar.teams = response_api.data.teamList;
          this.teams = this.globVar.teams;
        } catch (Error) {
          alert(Error.message);
        }
      },
      (error) => {
        this.loading.dismiss();
        alert("Error: Request failed-" + error.message);
      }
    );
  }





  addTeam(){
      let addTeamModal = this.modalCtrl.create(AddTeam, { userId: 8675309 });
      this.addTeamCalled = true;
      addTeamModal.onDidDismiss(() => {

        if (this.addTeamCalled) {
          this.addTeamCalled = false;
          this.loadTeams();
        }
        // This will be executed after the modal is dismissed...
      });
      addTeamModal.present();
  }

  loadTeam(index: any){
    this.globVar.selectedTeamIndex = index;
    this.globVar.team = this.globVar.teams[this.globVar.selectedTeamIndex];
    this.navCtrl.setRoot(TeamProfile);
  }


}





// export class MyTeam {
//   public searchTerm;
//   searchControl: FormControl;
//   films: Observable<any>;
//   selectedItem: any;
//   Tags:any;
//   icons: string[];
//   items: Array<{title: string, note: string}>;
//   itemtemp: Array<{title: string, note: string}>;
//   index: number;
//   pageno:number=0;
//   lastcallpageno:number;

//   constructor(public navCtrl: NavController, public navParams: NavParams,
 //public httpClient: HttpClient,public modalCtrl : ModalController,public http: Http) {
//     this.items = [];
//     const obj = {UserId: 1, ParentId: 1, Page: 0, PageSize: 10}
//     this.searchControl = new FormControl();
//     this.apipaggingcall(this.pageno);
//   }
//   ionViewDidLoad() {

//     this.searchControl.valueChanges.debounceTime(100).subscribe(search => {
//     this.items=this.itemtemp;
//         this.setFilteredItems();
//     });
// }
//   itemTapped(event, item) {
//     // That's right, we're pushing to ourselves!
//     // this.navCtrl.push(ListPage, {
//     //   item: item
//     // });
//     const index: number = this.items.indexOf(item);
//     alert(index);
//     this.items.splice(index,1);
//   }
//   itemTappededit(event, item) {
//     // That's right, we're pushing to ourselves!
//     // this.navCtrl.push(ListPage, {
//     //   item: item
//     // });
//     const index: number = this.items.indexOf(item);
//     alert(index);
//     this.items.splice(index,1,{
//       title: 'Item Dummy',
//       note: 'This is item # dummy'
//     });
//   }

//   public openModal(event, item){
//   this.index = this.items.indexOf(item);
//     var data = { message : item };
//     var modalPage = this.modalCtrl.create(ModelPage,data);
//     modalPage.onDidDismiss(data => {
//       console.log(data);
//       this.items.splice(this.index,1,{
//         title: data.title,
//         note: data.note
//       });
//  });
//     modalPage.present();
// }
// setFilteredItems() {
// this.items= this.items.filter((item) => {
//     return item.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
// });
// }
// doInfinite(infiniteScroll) {
//   if(this.lastcallpageno!=this.pageno)
//   {
//     this.apipaggingcall(this.pageno);
//     infiniteScroll.complete();
//   }else{

//     infiniteScroll.complete();
//     this.ionViewDidLoad();
//   }


// }

// apipaggingcall(pageno)
// {
//   this.lastcallpageno=pageno;
//   const obj = {UserId: 1, ParentId: 1, Page: pageno, PageSize: 10}
//   this.searchControl = new FormControl();
//  // this.films = this.httpClient.get('https://swapi.co/api/films');
//  var headers = new Headers();
//     headers.set('Content-type','application/json');
//       console.log('doing'+ JSON.stringify(obj));
//       debugger;
//        const uri = 'http://72.249.170.12/InsuranceCalicaApi/api/Quotation/GetQuotationListByTLTC';
//        this.http.post(uri,JSON.stringify(obj),{headers})
//  // this.films
//   .subscribe(data => {
//     debugger;
//     var json=data;
//     console.log('my data: ', json);
//   //   this.Tags=JSON.stringify(data);
//   //  // console.log('my data: ', this.Tags);
//   //  var datanew =JSON.parse(this.Tags);
//   if( data.json()['ResponseData'].length>0)
//   {
//     for (let item of data.json()['ResponseData'])
//     {
//       console.log("title",item.QuotationCode);
//       this.items.push({
//             title: 'Item ' + item.QuotationCode,
//             note: 'This is item #' + item.QuotationId
//           });
//     }
//    this.itemtemp =this.items
//     this.pageno++;
//     alert('call');
//   }
//   })
// }
// }
