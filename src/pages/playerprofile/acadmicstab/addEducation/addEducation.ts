import { Experiences } from "./../../../../model/experiences.model";
import { Component, ViewChild } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Platform,
  Keyboard
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../../../model/rosters.model";
import { Renderer } from "@angular/core";
import * as Constants from "../../../../model/constant.model";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { ToastController } from "ionic-angular";

@Component({
  selector: "page-addEducation",
  templateUrl: "addEducation.html"
})
export class AddEducation {
  @ViewChild("navbar") navBar: Navbar;
  public loading;
  teamImage = "assets/imgs/avatar.jpeg";
  startDate: String;
  endDate: String;
  institute = "";
  title = "";
  venue = "";
  location = "";
  currentWorking = false;
  isEdit = false;

  data: any = {};
  constructor(
    platform: Platform,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private navCtrl: NavController,
    private datePicker: DatePicker,
    private keyboard: Keyboard
  ) {
    if (params.get("data")) {
      this.isEdit = true;
      this.data = params.get("data");
    } else {
      this.isEdit = false;
      this.data = {
        Institute: "",
        Title: "",
        Location: "",
        StartDate: "",
        EndDate: "",
        isCurrentWorking: false
      };
    }

    console.log("UserId", this.data.Institute);
  }
  ionViewDidLoad(): void {}

  discard() {
    this.viewCtrl.dismiss();
  }
  save() {
    let obj: Experiences = this.data;

    this.viewCtrl.dismiss(obj);
  }

  datePickerfunc() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => console.log("Got date: ", date),
        err => console.log("Error occurred while getting date: ", err)
      );
  }
}
