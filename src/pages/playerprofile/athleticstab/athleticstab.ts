import { Stat100M } from './../../stats/stat100m/stat100m';
import { User } from "../../../model/user.model";
import {
  Url,
  DataServiceProvider
} from "../../../providers/data-service/data-service";
import * as Constants from "../../../model/constant.model";
import { coachProfile } from "../../../model/coachProfile.model";
import { Experiences } from '../../../model/experiences.model';
import { AddExp } from "../../userprofile/addExp/addExp";
import { Component, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { Http } from "@angular/http";
import { GooglePlus } from "@ionic-native/google-plus";
import { AppPreferences } from "@ionic-native/app-preferences";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Content, NavController, LoadingController, Events, ModalController, Navbar } from "ionic-angular";
import { Storage } from "@ionic/storage";



@Component({
  selector: "page-athleticstab",
  templateUrl: "athleticstab.html"
})
export class Athleticstab {
  @ViewChild('navbar') navBar: Navbar;
  @ViewChild(Content) content: Content;
  addExpCalled= false;
  experiences : Array <Experiences>= [];

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public items : any;

  /**
   * @name config
   * @type {any}
   * @public
   * @description     Defines an object allowing the interface properties to be accessed
   */


  public loading;
  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public publishEvent: Events,
    private storage: Storage,
    private datePicker: DatePicker,
    public modalCtrl: ModalController,

  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
  }

  discard(){
    this.navCtrl.pop();
  }

    goToSportStats(index:any){
      let addEduModal;
      if(index=Constants.sport100M){
        addEduModal = this.modalCtrl.create(Stat100M, { data: this.experiences[index] });
      }
      else if(index=Constants.sport1000M){
        addEduModal = this.modalCtrl.create(AddExp, { data: this.experiences[index] });
      }
      else if(index=Constants.sportMarathon){
        addEduModal = this.modalCtrl.create(AddExp, { data: this.experiences[index] });
      }
      else if(index=Constants.sportLongJump){
        addEduModal = this.modalCtrl.create(AddExp, { data: this.experiences[index] });
      }


      this.addExpCalled=true;
      addEduModal.onDidDismiss((data) => {
        if (this.addExpCalled) {
          this.addExpCalled = false;
          if (data) {
          this.experiences[index]=data;
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addEduModal.present();
    }
}
