import { PlayerProfile } from './../playerprofile';
import { User } from "../../../model/user.model";
import { Observable } from "rxjs/Observable";
import { Component, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import {Content, NavController, LoadingController,Navbar, Events, ModalController } from "ionic-angular";
import { GooglePlus } from "@ionic-native/google-plus";
import {
  Url,
  DataServiceProvider
} from "../../../providers/data-service/data-service";
import * as Constants from "../../../model/constant.model";
import { AppPreferences } from '@ionic-native/app-preferences';
import { coachProfile } from "../../../model/coachProfile.model";
import { Storage } from "@ionic/storage";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Experiences } from '../../../model/experiences.model';
import { AddExp } from "../../userprofile/addExp/addExp";
import { Player } from '../../../model/playerprofile.model';


@Component({
  selector: "page-personaltab",
  templateUrl: "personaltab.html"
})
export class PersonalTab {
  @ViewChild('navbar') navBar: Navbar;
  @ViewChild(Content) content: Content;

  public sports: any[] = Constants.sports;

  public username: string;
  public password: string;
  public error: string;
  passwordType: string = "password";
  paIcon: string = "eye-off";
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  userImage ="assets/imgs/avatar.jpeg";
  teamImage ="assets/imgs/team.png";
  profile: Player = new Player();
  profileSend: Player = new Player();
  Firstname = (this.globVar.user)?this.globVar.user.firstName:"N/A";;
  Lastname = (this.globVar.user)?this.globVar.user.lastName:"N/A";
  Email = (this.globVar.user)?this.globVar.user.email:"N/A";
  Phone =  (this.globVar.user)?this.globVar.user.phoneNumber:"N/A";

  addExpCalled= false;
  experiences : Array <Experiences>= [];

  isEdit:boolean = false;

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public items : any;

  /**
   * @name config
   * @type {any}
   * @public
   * @description     Defines an object allowing the interface properties to be accessed
   */


  public loading;
  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public publishEvent: Events,
    private storage: Storage,
    private datePicker: DatePicker,
    public modalCtrl: ModalController,


  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
    this.getPlayerProfile();
  }


  editView(status:boolean){
    this.isEdit=status;
  }

  ionViewDidEnter() {

  }




  getPlayerProfile(){
  //  this.loading = this.loadingCtrl.create({ content: "please wait..." });
  //  this.loading.present();
      this.api.getRequset(this.url.getPlayerProfileById + this.globVar.user.id).subscribe( (response_api) =>{
      if(response_api.status == Constants.OK)
       {
        this.profile = response_api.playerProfile;
        this.profileSend = response_api.playerProfile;
       }
       else{
        this.api.showToast("Failed: "+response_api.errors[0].code);
       }
       },
        error => {
        this.api.showToast("error"+error.message) ;
    });

  }




    addExperience() {
      let addExpModal = this.modalCtrl.create(AddExp, { data: null });
      this.addExpCalled=true;
      addExpModal.onDidDismiss((data) => {
        if (this.addExpCalled) {
          this.addExpCalled = false;
          if (data) {
          this.experiences[this.experiences.length]=data;
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addExpModal.present();
    }

    editExp(index:any){
      let addExpModal = this.modalCtrl.create(AddExp, { data: this.experiences[index] });
      this.addExpCalled=true;
      addExpModal.onDidDismiss((data) => {
        if (this.addExpCalled) {
          this.addExpCalled = false;
          if (data) {
          this.experiences[index]=data;
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addExpModal.present();
    }

    save(){
      this.loading = this.loadingCtrl.create({ content: "please wait..." });
      this.loading.present();
      let obj = { FirstName : this.Firstname ,
                  LastName: this.Lastname ,
                  Email: this.Email.toLowerCase() ,
                  Id : this.globVar.user.id,
                  PhoneNumber: this.Phone
                };

      this.api.postRequset(this.url.updateProfile ,obj).subscribe( (response_api) =>{
        this.loading.dismiss();
        if(response_api.status == Constants.OK)
         {
              this.savePersonalInfo();
         }
         else{
           this.api.showToast("Failed: "+response_api.errors[0].code);
         }
         },
           error => {
          this.loading.dismiss();
          this.api.showToast("Error: "+error.message+obj.Id) ;
      });
    }

    savePersonalInfo(){
      this.loading = this.loadingCtrl.create({ content: "please wait..." });

      this.loading.present();
      let obj = this.profileSend;

      this.api.postRequset(this.url.updateCoachProfile ,obj).subscribe( (response_api) =>{
        this.loading.dismiss();
        if(response_api.status == Constants.OK)
         {
           this.profile=this.profileSend
          this.getUser();
         }
         else{
           this.api.showToast("Failed: ");
         }
         },
           (error) => {
          this.loading.dismiss();
          this.api.showToast("Error: "+error.message) ;
      });
    }

    getUser(){
      //  this.loading = this.loadingCtrl.create({ content: "please wait..." });
      //  this.loading.present();
          this.api.getRequset(this.url.getUserbyId + this.globVar.user.id).subscribe( (response_api) =>{
      //    this.loading.dismiss();
          if(response_api.status == Constants.OK)
           {
            this.globVar.user = response_api.users;
            this.storage.set ( 'user',  this.globVar.user);
            this.publishEvent.publish("userProfile",{action:"loadUserProfile"} );
            this.editView(false);
           }
           else{
            this.api.showToast("Failed: "+response_api.errors[0].code);
           }
           },
            error => {
            this.api.showToast("error"+error.message) ;
        });
      }
}
