import { Acadmicstab  } from './acadmicstab/acadmicstab';
import { PersonalTab } from './personaltab/personaltab';
import { User } from "../../model/user.model";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";
import { coachProfile } from "../../model/coachProfile.model";
import { Experiences } from '../../model/experiences.model';
import { AddExp } from "../userprofile/addExp/addExp";
import { Component, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { Http } from "@angular/http";
import { GooglePlus } from "@ionic-native/google-plus";
import { AppPreferences } from "@ionic-native/app-preferences";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Content, NavController, LoadingController, Events, ModalController, Navbar } from "ionic-angular";
import { Athleticstab } from './athleticstab/athleticstab';
import { Storage } from "@ionic/storage";


@Component({
  selector: "page-playerprofile",
  templateUrl: "playerprofile.html"
})
export class PlayerProfile {
  @ViewChild('navbar') navBar: Navbar;
  @ViewChild(Content) content: Content;
  tab1Root = PersonalTab;
  tab2Root = Acadmicstab;
  tab3Root = Athleticstab;
  public sports: any[] = Constants.sports;

  public username: string;
  public password: string;
  public error: string;
  passwordType: string = "password";
  paIcon: string = "eye-off";
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  userImage ="assets/imgs/avatar.jpeg";
  teamImage ="assets/imgs/team.png";
  Firstname = (this.globVar.user)?this.globVar.user.firstName:"N/A";;
  Lastname = (this.globVar.user)?this.globVar.user.lastName:"N/A";
  Email = (this.globVar.user)?this.globVar.user.email:"N/A";
  Phone =  (this.globVar.user)?this.globVar.user.phoneNumber:"N/A";

  addExpCalled= false;
  experiences : Array <Experiences>= [];

  isEdit:boolean = false;

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public items : any;

  /**
   * @name config
   * @type {any}
   * @public
   * @description     Defines an object allowing the interface properties to be accessed
   */


  public loading;
  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public publishEvent: Events,
    private storage: Storage,
    private datePicker: DatePicker,
    public modalCtrl: ModalController,


  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {

  }


  editView(status:boolean){
    this.globVar.editPlayerPersonal=status;
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
      ///here you can do wathever you want to replace the backbutton event
      if(this.isEdit === true)
      {

        this.editView(false);
      }
      else{
        this.discard();
      }
    };
  }

  discard(){
    this.navCtrl.pop();
  }

    datePickerfunc() {
      this.datePicker
        .show({
          date: new Date(),
          mode: "date",
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        })
        .then(
          date => console.log("Got date: ", date),
          err => console.log("Error occurred while getting date: ", err)
        );
    }

    // save(){
    //   this.loading = this.loadingCtrl.create({ content: "please wait..." });
    //   this.loading.present();
    //   let obj = { FirstName : this.Firstname ,
    //               LastName: this.Lastname ,
    //               Email: this.Email.toLowerCase() ,
    //               Id : this.globVar.user.id,
    //               PhoneNumber: this.Phone
    //             };

    //   this.api.postRequset(this.url.updateProfile ,obj).subscribe( (response_api) =>{
    //     this.loading.dismiss();
    //     if(response_api.status == Constants.OK)
    //      {
    //           //this.savePersonalInfo();
    //      }
    //      else{
    //        this.api.showToast("Failed: "+response_api.errors[0].code);
    //      }
    //      },
    //        error => {
    //       this.loading.dismiss();
    //       this.api.showToast("Error: "+error.message+obj.Id) ;
    //   });
    // }





}
