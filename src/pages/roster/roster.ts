import { Component } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Events,
  Platform
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../model/rosters.model";
import { Renderer } from "@angular/core";
import { AddRoster } from "../rosterstab/addroster";
import * as Constants from "../../model/constant.model";
import { Team } from "../../model/team.model.";

@Component({
  selector: "page-roster",
  templateUrl: "roster.html"
})
export class Rosters {
  itemImage = "assets/imgs/player.png";
  isSelctionView : boolean= false;
  addRosterCalled = false;
   sports : any[] = Constants.sports;
  team : Team = this.globVar.team;
  teamImage = "assets/imgs/team.png";
  teamName: String = this.team? this.team.name:"";
  sport:String=this.team?this.sports[ this.team.sportsId]:"";

  loading;
  public rosters: Array<Roster> = [];
  constructor(
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public publishEvent: Events
  )
  {
    this.subscribeEvents();
  }

  ionViewDidLoad(): void {
    this.loadTeamRosters();
  }

  subscribeEvents(){
    if(!this.globVar.rosterSubcribeflag)
    {
      this.globVar.rosterSubcribeflag=true;
      this.publishEvent.subscribe('Team', (data) =>{
      if(data.action=="teamChanged")
      {
        this.loadTeamRosters();
      }

    });
  }
  }


  loadTeamRosters() {
    // this.loading = this.loadingCtrl.create({ content: "please wait..." });
    // this.loading.present();
    this.api.getRequset(this.url.getTeamRosters(this.team.id,this.globVar.user.id)
      )
      .subscribe(
        response_api => {
          try {
          //  this.loading.dismiss();
            this.rosters=[];
            this.rosters = response_api.data.athleteList;
            // alert("teams"+response_api.data.teamList.length );
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
         // this.loading.dismiss();
          alert("Error: Request failed-" + error.message);
        }
      );
  }

  addRosters() {
    let addRosterModal = this.modalCtrl.create(AddRoster, { team: this.team });
    this.addRosterCalled=true;
    addRosterModal.onDidDismiss(() => {
                  if (this.addRosterCalled) {
                    this.addRosterCalled = false;
                    this.loadTeamRosters();
                  }
                  // This will be executed after the modal is dismissed...
                });
                addRosterModal.present();
  }

  changeTeam(index:any){
    this.isSelctionView = false;
    this.team = this.globVar.teams[index];
    this.teamName = this.team? this.team.name:"";
    this.sport=this.team?this.sports[ this.team.sportsId]:"";
    this.loadTeamRosters();
  }
}
