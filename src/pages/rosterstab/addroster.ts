import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Keyboard
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Renderer } from "@angular/core";
import { Team } from "../../model/team.model.";

@Component({
  selector: "page-addroster",
  templateUrl: "addroster.html"
})
export class AddRoster {
  @ViewChild("navbar") navBar: Navbar;

  teamImage = "assets/imgs/player.png";
  emailId: String = "";
  firstName: String = "";
  lastName: String = "";
  jersyNo: String = "";
  palyerId: String = "";
  loading;
  private list: Array<any> = [];
  public countries: Array<any> = [];
  team : Team ;
  constructor(
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private keyboard: Keyboard
  ) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement,
     "my-popup",
      true);
    this.team = params.get("team");
  }

  discard() {
    this.viewCtrl.dismiss();
  }

  save() {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let obj = {
      TeamId: this.team.id,
      JerseyNo: this.jersyNo,
      AspnetuserId: this.palyerId
    };
    this.api.postRequset(this.url.addAthlete, obj).subscribe(
      (response) => {
        this.loading.dismiss();
        this.api.showToast("Player Added to the team");
        this.viewCtrl.dismiss();
      },
      error => {
        this.loading.dismiss();
        this.api.showToast(error.message)
      }
    );
  }

  add(item: any) {
    this.countries = [];
    this.countries.length = 0;
    this.emailId = item.email;
    this.firstName = item.firstName;
    this.lastName = item.lastName;
    this.palyerId = item.id;
  }

  removeFocus() {
    this.keyboard.close();
  }

  search() {
    if (this.emailId.trim().length == 0) {
      this.countries = [];
      return;
    }

    this.api
      .getRequset(this.url.getPlayerList + this.emailId)
      .subscribe(response_api => {
        try {
          this.list = response_api.data.playerList;
          // if (!this.emailId.trim().length || !this.keyboard.isOpen()) {
          if (!this.emailId.trim().length) {
            this.countries = [];
            return;
          }
          this.countries = this.list.filter(item =>
            item.email.toUpperCase().includes(this.emailId.toUpperCase())
          );
        } catch (Error) {
          console.log("" + Error);

        }
      });
  }
}
