import { Component } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Events,
  Platform
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../model/rosters.model";
import { Renderer } from "@angular/core";
import { AddRoster } from "./addroster";

@Component({
  selector: "page-rosterstab",
  templateUrl: "rosterstab.html"
})
export class RostersTab {
  itemImage = "assets/imgs/player.png";
  addRosterCalled = false;
  loading;
  public rosters: Array<Roster> = [];
  constructor(
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public publishEvent: Events
  )
  {
    this.subscribeEvents();
  }

  ionViewDidLoad(): void {
    this.loadTeamRosters();
  }

  subscribeEvents(){
    if(!this.globVar.rosterSubcribeflag)
    {
      this.globVar.rosterSubcribeflag=true;
      this.publishEvent.subscribe('Team', (data) =>{
      if(data.action=="teamChanged")
      {
        this.loadTeamRosters();
      }

    });
  }
  }


  loadTeamRosters() {
    // this.loading = this.loadingCtrl.create({ content: "please wait..." });
    // this.loading.present();
    if(!this.globVar.team)
        return;
    this.api.getRequset(this.url.getTeamRosters(this.globVar.team.id,this.globVar.user.id)
      )
      .subscribe(
        response_api => {
          try {
          //  this.loading.dismiss();
            this.rosters=[];
            this.rosters = response_api.data.athleteList;
            // alert("teams"+response_api.data.teamList.length );
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
         // this.loading.dismiss();
          alert("Error: Request failed-" + error.message);
        }
      );
  }

  addRosters() {
    let addRosterModal = this.modalCtrl.create(AddRoster, { team: this.globVar.team });
    this.addRosterCalled=true;
    addRosterModal.onDidDismiss(() => {
                  if (this.addRosterCalled) {
                    this.addRosterCalled = false;
                    this.loadTeamRosters();
                  }
                  // This will be executed after the modal is dismissed...
                });
                addRosterModal.present();
  }
}
