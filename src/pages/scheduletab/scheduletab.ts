import { Component } from "@angular/core";
import { Http } from "@angular/http";
import {NavController, LoadingController,ModalController, Platform, Events } from "ionic-angular";
import { GooglePlus } from "@ionic-native/google-plus";
import {  Url,  DataServiceProvider } from "../../providers/data-service/data-service";
import { AppPreferences } from '@ionic-native/app-preferences';
import { Event } from "../../model/event.model";
import { AddEvent } from '../addevent/addevent';


@Component({
  selector: 'page-scheduletab',
  templateUrl: 'scheduletab.html'
})

export class ScheduleTab {
  loading;
  addEventCalled=false;
  events: Array<Event> = [];
  isUpcomming:boolean= true;
  itemImage = "assets/imgs/game.png";


  constructor(platform: Platform,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public publishEvent: Events
    ) {

      platform.ready().then(() => {
        this.subscribeEvents();
      });



  }

  subscribeEvents(){
    if(!this.globVar.scheduleSubcribeflag)
    {
      this.globVar.scheduleSubcribeflag=true;
    this.publishEvent.subscribe('Team', (data) =>{
      if(data.action=="teamChanged")
      {
        this.loadEvents();
      }
      else if(data.action=="firstLoad")
      {
                //  this.loadEvents();

      }
    });
  }
  }

  createEvent(){
      let addEventeModal = this.modalCtrl.create(AddEvent, { userId: 8675309 });
      this.addEventCalled=true;
      addEventeModal.onDidDismiss((data) => {
        if (this.addEventCalled) {
          this.addEventCalled = false;
          if(data)
          {
            this.loadEvents();
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addEventeModal.present();
  }

  ionViewDidLoad(): void {
    setTimeout(() => {
      if(this.globVar.team)
        {
          this.loadEvents();
        }
        else{
          setTimeout(() => {
            if(this.globVar.team)
              {
                this.loadEvents();
              }
              else{
                setTimeout(() => {
                  if(this.globVar.team)
                    {
                      this.loadEvents();
                    }
                    else{
                     // this.loadEvents();
                    }
                },1500);
              }
          },1500);
        }
    },1000);

  }


  loadEvents(): void {
    if(!this.globVar.team)
    return;
   this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let event_url= this.url.getScheduleEvent + this.globVar.user.id+"&teamId=" + this.globVar.team.id;
    if(this.isUpcomming)
    event_url= event_url+"&gameType=2";
    else
    event_url= event_url+"&gameType=1";
  //"0116ffc9-08ce-4b0f-a441-cde674103c58"
    this.api.getRequset(event_url).subscribe(
      (response_api) => {
       this.loading.dismiss();
        try {
          this.events=[];
          this.events = response_api.data.eventList;
        } catch (Error) {
          alert(Error.message);
        }
      },
      (error) => {
        this.loading.dismiss();
        alert("Error: Request failed-" + error.message);
      }
    );
  }

  selectionChange(value:boolean){
    this.isUpcomming = value;
    this.loadEvents();
  }
  openScore(eventItem:Event){
    this.publishEvent.publish("teamProfile", {action:"goToScore",event: eventItem });
  }

}
