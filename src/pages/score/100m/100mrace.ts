import { Component, ViewChild } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../../model/rosters.model";
import { Renderer } from "@angular/core";
import * as Constants from "../../../model/constant.model";
import { Event } from "../../../model/event.model";
import { RaceScore } from "../../../model/racescore.model.";

@Component({
  selector: "page-100mrace",
  templateUrl: "100mrace.html"
})
export class Race100M {
  @ViewChild("navbar") navBar: Navbar;
  itemImage = "assets/imgs/avatar.jpeg";
  addRosterCalled = false;
  public sports : any[] = Constants.sports;
  eRaceTime:string="";
  eRaceTimemili:string="";
  eRank:string="";
  loading;
  isEdit: boolean = false;
  event: Event;
  playerIndex=0;
  isNoPlayer:boolean;

  public rosters: Array<Roster> = [];
  public scoredRosters: Array<Roster> = [];

  public scores: Array<RaceScore> = [];
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController
  ) {
    this.event = navParams.get("event");
  }
  ionViewDidLoad(): void {
   // this.loadTeamRosters();
    this.loadTeamScore();
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
      this.discard();
    };
  }

  loadTeamRosters() {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    this.api
      .getRequset(
        this.url.getTeamRosters(this.globVar.team.id, this.globVar.user.id)
      )
      .subscribe(
        response_api => {
          try {
            this.loading.dismiss();
            this.rosters = response_api.data.athleteList;
            if(this.rosters.length>0)
            {
              this.isNoPlayer = false;
              this.loadTeamScore();
            }
            else{
              this.isNoPlayer = true;
              this.rosters = [];
              this.api.showToast("Please, Add the atheltes to the Team");
            }
            // alert("teams"+response_api.data.teamList.length );
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
          this.loading.dismiss();
          this.api.showToast("Error: Request failed-" + error.message);
        }
      );
  }
  compareFn(e1: Roster, e2: Roster): boolean {
    return e1 && e2 ? e1.id === e2.id : e1 === e2;
  }
  loadTeamScore() {
  //  this.loading = this.loadingCtrl.create({ content: "please wait..." });
   // this.loading.present();
    this.api
      .getRequset(
        this.url.getRaceScore +
          this.event.id +
          "&coachId=" +
          this.globVar.user.id )
      .subscribe(
        response_api => {
          try {
           // this.loading.dismiss();
            this.scores = response_api.data.scoreList;
          //  this.scores.sort((a,b) => a.raceTime.localeCompare(b.raceTime));


            // for (var i = 0; i < this.rosters.length; i++) {
            //   let scoreiItem = this.scores.find(
            //     score => score.playerId === this.rosters[i].userId
            //       );
            //   if (scoreiItem) {
            //     this.rosters[i].eRaceTime = scoreiItem.raceTime
            //     this.rosters[i].eRank = scoreiItem.rank;
            //     this.rosters[i].RaceTime = scoreiItem.raceTime;
            //     this.rosters[i].Rank = scoreiItem.rank;
            //    // this.scoredRosters[i]=rosteritem;
            //   }
            // }
            // alert("teams"+response_api.data.teamList.length );
          } catch (Error) {
            alert(Error.message);
          }
        },
        error => {
          this.loading.dismiss();
          alert("Error: Request failed-" + error.message);
        }
      );
  }

  addRosters() {}

  discard() {
    this.navCtrl.pop();
  }

  editView(status: boolean) {
    this.isEdit =status;

    if ( !status ) {
      this.saveTheScore( status );
    }
  }

  saveTheScore(index: any) {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();

    let scoreArray: Array<any> = [];

    for (var i = 0; i < this.scores.length; i++) {
      let obj = {
        TeamId: this.globVar.team.id,
        EventId: this.event.id,
        AspnetuserId: this.scores[i].playerId,
        RaceTime: this.scores[i].eRaceTime,
        Rank:  i+1,
        CreatedBy: this.globVar.user.id
      };

      scoreArray[i] = obj;

    }



    // let obj = {
    //   TeamId: this.globVar.team.id,
    //   EventId: this.event.id,
    //   AspnetuserId: this.scores[index].playerId,
    //   RaceTime: this.scores[index].eRaceTime,
    //   Rank: this.scores[index].eRank,
    //   CreatedBy: this.globVar.user.id
    // };

    this.api.postRequset(this.url.addRaceScore, scoreArray).subscribe(
      response_api => {
        this.loading.dismiss();
        if (response_api.status == Constants.OK) {
          this.loadTeamScore();
          this.api.showToast("Score Saved");

        } else {
          this.api.showToast("Failed: " + response_api.errors[0].code);
        }
      },
      error => {
        this.loading.dismiss();
        this.api.showToast("Failed: " + error.message);
      }
    );
  }

  add(){

      let rosteritem = this.scoredRosters.find(
        rost => rost.userId === this.rosters[this.playerIndex].userId
      );
      if (rosteritem) {
        this.api.showToast("Score already added");
        return;
      }

    if(this.eRaceTime =="" || this.eRank ==""){
      this.api.showToast("Time and rank both are required");
      return;
    }

    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let scoreArray: Array<any> = [];
    for (var i = 0; i < this.scores.length; i++) {
      let obj = {
        TeamId: this.globVar.team.id,
        EventId: this.event.id,
        AspnetuserId: this.scores[i].playerId,
        RaceTime: this.scores[i].eRaceTime,
        Rank:  this.scores[i].eRank,
        CreatedBy: this.globVar.user.id
      };

      scoreArray[i] = obj;

    }

    this.api.postRequset(this.url.addRaceScore, scoreArray).subscribe(
      response_api => {
        this.loading.dismiss();
        if (response_api.status == Constants.OK) {
          this.eRaceTime="";
          this.eRank = "";
          this.loadTeamScore();
          this.api.showToast("Score Added");

        } else {
          this.api.showToast("Failed: " + response_api.errors[0].code);
        }
      },
      error => {
        this.loading.dismiss();
        this.api.showToast("Failed: " + error.message);
      }
    );
  }

  stopWatch(){

  }
}


// loadTeamScore() {
//   //  this.loading = this.loadingCtrl.create({ content: "please wait..." });
//    // this.loading.present();
//     this.api
//       .getRequset(
//         this.url.getRaceScore +
//           this.event.id +
//           "&coachId=" +
//           this.globVar.user.id )
//       .subscribe(
//         response_api => {
//           try {
//            // this.loading.dismiss();
//             this.scores = response_api.data.scoreList;

//             for (var i = 0; i < this.scores.length; i++) {
//               let rosteritem = this.rosters.find(
//                 rost => rost.userId === this.scores[i].playerId
//               );
//               if (rosteritem) {
//                 rosteritem.eRaceTime = this.scores[i].raceTime
//                 rosteritem.eRank = this.scores[i].rank;
//                 rosteritem.RaceTime = this.scores[i].raceTime;
//                 rosteritem.Rank = this.scores[i].rank;

//                 this.scoredRosters[i]=rosteritem;
//               }
//             }

//             // alert("teams"+response_api.data.teamList.length );
//           } catch (Error) {
//             alert(Error.message);
//           }
//         },
//         error => {
//           this.loading.dismiss();
//           alert("Error: Request failed-" + error.message);
//         }
//       );
//   }
