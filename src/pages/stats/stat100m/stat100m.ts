import { Stat100m } from './../../../model/stat100m.model';
import { Experiences } from "../../../model/experiences.model";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Platform,
  Keyboard
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../../model/rosters.model";
import { Renderer } from "@angular/core";
import * as Constants from "../../../model/constant.model";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { ToastController } from "ionic-angular";
import { Component,ViewChild} from "@angular/core";

@Component({
  selector: "page-stat100m",
  templateUrl: "stat100m.html"
})
export class Stat100M {
  @ViewChild("navbar") navBar: Navbar;
  public loading;
  public sports: any[] = Constants.sports;
  sport = this.sports[0];

  stat:Stat100m= new Stat100m();
  teamImage = "assets/imgs/avatar.jpeg";
  startDate: String;
  endDate: String;
  institute = "";
  title = "";
  venue = "";
  location = "";
  currentWorking = false;
  isEdit = false;

  data: any = {};
  constructor(
    platform: Platform,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private navCtrl: NavController,
    private datePicker: DatePicker,
    private keyboard: Keyboard
  ) {
    if (params.get("data")) {
      this.isEdit = true;
      this.data = params.get("data");
    } else {
      this.isEdit = false;
      this.data = {
        Institute: "",
        Title: "",
        Location: "",
        StartDate: "",
        EndDate: "",
        isCurrentWorking: false
      };
    }
    console.log("UserId", this.data.Institute);
  }
  ionViewDidLoad(): void {

  }


  save() {
    let obj: Experiences = this.data;

    this.viewCtrl.dismiss(obj);
  }

  datePickerfunc() {
    this.datePicker
      .show({
        date: new Date(),
        mode: "date",
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
      })
      .then(
        date => console.log("Got date: ", date),
        err => console.log("Error occurred while getting date: ", err)
      );
  }

  editView(status:boolean){
    this.isEdit=status;
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
      ///here you can do wathever you want to replace the backbutton event
      if(this.isEdit === true)
      {
        this.editView(false);
      }
      else{
        this.discard();
      }
    };
  }

  discard(){
    this.navCtrl.pop();
  }


  loadStats() {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    this.api
      .getRequset(this.url.getPlayer100meterStatistics + this.globVar.user.id)
      .subscribe(
        response_api => {
          this.loading.dismiss();
          try {
            //this.assistantCoach = response_api.data.assistantList;
          } catch (Error) {
            //this.api.showToast("No Assistant Coaches");
          }
        },
        error => {
          this.loading.dismiss();
          this.api.showToast("Request failed");
        }
      );
  }


}
