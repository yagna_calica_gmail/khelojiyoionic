import { TeamProfile } from "../teamprofile/teamprofile";
import { User } from "../../model/user.model";
import { Observable } from "rxjs/Observable";
import { RegisterPage } from "../Register/register";
import { Component, ViewChild } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { AngularFireDatabase } from "angularfire2/database";
import { FirebaseProvider } from "../../providers/firebase/firebase";
import {Content, NavController, LoadingController,Navbar, Events } from "ionic-angular";
import { HomePage } from "../home/home";
import { ForgotPage } from "../forgot/forgot";
import * as firebase from "firebase";
import { firebaseConfig } from "../../app/app.module";
import { GooglePlus } from "@ionic-native/google-plus";
import { environment } from "../../environments/environment";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from '@ionic-native/app-preferences';


@Component({
  selector: "page-stopwatch",
  templateUrl: "stopwatch.html"
})
export class StopWatch {
  @ViewChild('navbar') navBar: Navbar;
  public sports: any[] = Constants.sports;
  public username: string;
  public password: string;
  public error: string;
  loginError: string;
  uber: Observable<any>;
  userImage ="assets/imgs/avatar.jpeg";
  teamImage ="assets/imgs/team.png";
  @ViewChild(Content) _content: Content;
  isEdit:boolean = false;
  public timeBegan = null;
  public timeStopped:any = null;
  public stoppedDuration:any = 0;
  public started = null;
  public running = false;
  public blankTime = "00:00.000";
  public time = "00:00.000";
  public laps : Array<string>=[];

  public loading;
  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public publishEvent: Events


  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
  }


  editView(status:boolean){
    this.isEdit=status;
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.pop();
    };
  }

  discard(){
    this.navCtrl.pop();
  }

  start() {
    if(this.running) return;
    if (this.timeBegan === null) {
        this.reset();
        this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      let newStoppedDuration:any = (+new Date() - this.timeStopped)
      this.stoppedDuration = this.stoppedDuration + newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 10);
      this.running = true;
    }
    stop() {
      this.running = false;
      this.timeStopped = new Date();
      clearInterval(this.started);
   }
    reset() {
      this.running = false;
      clearInterval(this.started);
      this.stoppedDuration = 0;
      this.timeBegan = null;
      this.timeStopped = null;
      this.laps=[];
      this.time = this.blankTime;
    }

    lap(){
      this.laps[this.laps.length]= this.time;
    }
    zeroPrefix(num, digit) {
      let zero = '';
      for(let i = 0; i < digit; i++) {
        zero += '0';
      }
      return (zero + num).slice(-digit);
    }
    clockRunning(){
      let currentTime:any = new Date()
      let timeElapsed:any = new Date(currentTime - this.timeBegan - this.stoppedDuration)
      let hour = timeElapsed.getUTCHours()
      let min = timeElapsed.getUTCMinutes()
      let sec = timeElapsed.getUTCSeconds()
      let ms = timeElapsed.getUTCMilliseconds();




    this.time =
      this.zeroPrefix(hour, 2) + ":" +
      this.zeroPrefix(min, 2) + ":" +
      this.zeroPrefix(sec, 2) + "." +
      this.zeroPrefix(ms, 3);
    };


}
