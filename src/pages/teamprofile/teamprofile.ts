import { sports } from "./../../model/constant.model";
import { ScheduleTab } from "./../scheduletab/scheduletab";
import { Team } from "./../../model/team.model.";
import { TeamProfileTab } from "./../teamprofiletab/teamprofiletab";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  LoadingController,
  Nav,
  ModalController
} from "ionic-angular";
import { Platform } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import * as Constants from "../../model/constant.model";
import { RostersTab } from "../rosterstab/rosterstab";
import { AddTeam } from "../addteam/addteam";
import { Events } from "ionic-angular";
import { Race100M } from "../score/100m/100mrace";
import { Event } from "../../model/event.model";

@Component({
  selector: "page-home",
  templateUrl: "teamprofile.html"
})
export class TeamProfile {
  @ViewChild(Nav) nav: Nav;
  isPlayer = this.globVar.isPlayer;
  public sports: any[] = Constants.sports;
  isSelctionView: boolean = false;
  tab1Root = ScheduleTab;
  tab2Root = RostersTab;
  tab3Root = TeamProfileTab;
  teamImage = "assets/imgs/team.png";
  teamName: String = "";
  sport: String = "";
  loading;
  addTeamCalled = false;

  constructor(
    platform: Platform,
    public loadingCtrl: LoadingController,
    public api: DataServiceProvider,
    public url: Url,
    public navCtrl: NavController,
    public globVar: Url,
    public modalCtrl: ModalController,
    public publishEvent: Events,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      this.subscribeEvents();
      splashScreen.hide();
      this.viewDidLoad();
    });
  }

  subscribeEvents() {
    if (!this.globVar.teamProfSubcribeflag) {
      this.globVar.teamProfSubcribeflag = true;
      this.publishEvent.subscribe("teamProfile", data => {
        if (data.action == "loadTeams") {
          this.loadTeams();
        } else if (data.action == "goToScore") {
          this.goToScore(data.event);
        }
      });
    }
  }

  viewDidLoad(): void {
    this.publishEvent.publish("userProfile", { action: "loadUserProfile" });
    setTimeout(() => {
      this.loadTeams();
    }, 500);
  }

  loadTeams(): void {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    //"0116ffc9-08ce-4b0f-a441-cde674103c58"
    this.api.getRequset(this.url.getTeamsbyId + this.globVar.user.id).subscribe(
      response_api => {
        // var response_api  = new APIResponse(response);
        //alert("successiotyufg"+ response_api.status);
        this.loading.dismiss();
        try {
          this.globVar.teams = [];
          this.globVar.teams = response_api.data.teamList;
          if (this.globVar.teams.length == 0) {
            if (!this.globVar.isPlayer) {
              let addTeamModal = this.modalCtrl.create(AddTeam, {
                userId: this.globVar.user.id
              });
              this.addTeamCalled = true;
              addTeamModal.onDidDismiss(() => {
                if (this.addTeamCalled) {
                  this.addTeamCalled = false;
                  //this.loadTeams();
                  this.navCtrl.setRoot(this.navCtrl.getActive().component);
                }
              });
              addTeamModal.present();
            }
            else {

            }
          } else {
            this.globVar.team = this.globVar.teams[
              this.globVar.selectedTeamIndex
            ];
            this.teamName = this.globVar.team.name;
            this.sport = sports[this.globVar.team.sportsId];
            this.publishEvent.publish("Team", { action: "firstLoad" });
          }
        } catch (Error) {
          alert(Error.message);
        }
      },
      error => {
        this.loading.dismiss();
        alert("Error: Request failed-" + error.message);
      }
    );
  }

  onSelectChange() {
    this.globVar.team = this.globVar.teams[this.globVar.selectedTeamIndex];
    this.publishEvent.publish("Team", { action: "teamChanged" });
  }

  changeTeam(index: any) {
    this.globVar.selectedTeamIndex = index;
    this.globVar.team = this.globVar.teams[this.globVar.selectedTeamIndex];
    this.teamName = this.globVar.team.name;
    this.sport = sports[this.globVar.team.sportsId];
    this.publishEvent.publish("Team", { action: "teamChanged" });
    this.isSelctionView = false;
  }

  goToScore(eventItem: Event) {
    this.navCtrl.push(Race100M, { event: eventItem });
  }
}
