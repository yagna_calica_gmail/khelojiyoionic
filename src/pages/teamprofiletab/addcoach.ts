import { Component, ViewChild } from "@angular/core";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  ViewController,
  NavParams,
  Keyboard
} from "ionic-angular";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import { AppPreferences } from "@ionic-native/app-preferences";
import { Roster } from "../../model/rosters.model";
import { Renderer } from "@angular/core";

@Component({
  selector: "page-addcoach",
  templateUrl: "addcoach.html"
})
export class AddCoach {

  @ViewChild('navbar') navBar: Navbar;

  teamImage =  "assets/imgs/coach.png";
  emailId: String = "";
  firstName: String ="";
  lastName : String = "";
  jersyNo : String = "";
  coachId: String = "";
  loading;
  private list: Array<any> = [];
  public countries: Array<any> = [];

  constructor(
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public params: NavParams,
    private navCtrl: NavController,
    private keyboard: Keyboard
  )

  {
    this.renderer.setElementClass(
      viewCtrl.pageRef().nativeElement,
      "my-popup",
      true
    );
    console.log("UserId", params.get("userId"));
  }

  discard() {
    this.viewCtrl.dismiss();
  }
  save() {

    if(this.coachId==""){
      this.api.showToast("please provide registered coach");
      return;
    }

    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let obj = {
      TeamId: this.globVar.team.id,
      JerseyNo: this.jersyNo,
      AspnetuserId: this.coachId
    };
    this.api.postRequset(this.url.addAssitantCoach, obj).subscribe(
      (response) => {
        this.loading.dismiss();
        this.api.showToast("Coach Added to the team");
        this.viewCtrl.dismiss(obj);
      },
      error => {
        this.loading.dismiss();
        alert("Bad Request");
      }
    );
  }

  add(item: any) {
    this.countries = [];
    this.countries.length = 0;
    this.emailId = item.email;
    this.firstName = item.firstName;
    this.lastName = item.lastName;
    this.coachId = item.id;
  }

  removeFocus() {
    this.keyboard.close();
  }

  search() {
    if (this.emailId.trim().length == 0) {
      this.countries = [];
      return;
    }

    this.api
      .getRequset(this.url.getAssistantCoachList + this.emailId)
      .subscribe(response_api => {
        try {
          this.list = response_api.data.playerList;
          // if (!this.emailId.trim().length || !this.keyboard.isOpen()) {
          if (!this.emailId.trim().length) {
            this.countries = [];
            return;
          }
          this.countries = this.list.filter(item =>
            item.email.toUpperCase().includes(this.emailId.toUpperCase())
          );
        } catch (Error) {
          //this.api.showToast("Request Failed, Please check the data");
        }
      });
  }

}
