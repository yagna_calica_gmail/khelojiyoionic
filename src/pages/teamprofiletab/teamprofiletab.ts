import { sports } from "./../../model/constant.model";
import { AddCoach } from "./addcoach";
import { User } from "../../model/user.model";
import { Observable } from "rxjs/Observable";
import { Component, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import {
  Content,
  NavController,
  LoadingController,
  Navbar,
  ModalController,
  Platform
} from "ionic-angular";
import { GooglePlus } from "@ionic-native/google-plus";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from "@ionic-native/app-preferences";
import { AssistantCoach } from "../../model/AssistantCoach.model";
import { Events } from "ionic-angular";
import { Team } from "../../model/team.model.";

@Component({
  selector: "page-teamprofiletab",
  templateUrl: "teamprofiletab.html"
})
export class TeamProfileTab {
  @ViewChild("navbar") navBar: Navbar;
  public username: string;
  public password: string;
  public error: string;
  public sports: any[] = Constants.sports;
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  teamImage = "assets/imgs/team.png";
  coachImage = "assets/imgs/coach.png";

  teamName = this.globVar.team ? this.globVar.team.name : "";
  sportName = sports[this.globVar.team ? this.globVar.team.sportsId : "1"];
  @ViewChild(Content) _content: Content;
  isEdit: boolean = false;
  isAddCoachCalled: boolean = false;

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public assistantCoach: Array<AssistantCoach>;

  public loading;
  constructor(
    platform: Platform,
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public modalCtrl: ModalController,
    public publishEvent: Events
  ) {
    platform.ready().then(() => {
      this.subscribeEvents();
    });
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
    this.loadTeamCoaches();
  }

  subscribeEvents() {
    if (!this.globVar.teamtabSubcribeflag) {
      this.globVar.teamtabSubcribeflag = true;
      this.publishEvent.subscribe("Team", data => {
        if (data.action == "teamChanged") {
          this.loadTeamCoaches();
        }
      });
    }
  }
  editView(status: boolean) {
    this.isEdit = status;
  }

  ionViewDidEnter() {}

  discard() {
    this.navCtrl.pop();
  }

  save() {
    debugger;
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
   // this.loading.present();
    let obj = {
      Name: this.teamName,
      Id: this.globVar.team.id
    };
    this.api.postRequset(this.url.updateTeam, obj).subscribe(
      () => {
       // this.loading.dismiss();
        this.api.showToast("Team Profile Saved");
      //  this.globVar.teams[this.globVar.selectedTeamIndex].name = this.teamName;
        //this.globVar.team = this.globVar.teams[this.globVar.selectedTeamIndex];
       this.publishEvent.publish("teamProfile", { action: "loadTeams" });
        this.editView(false);
      },
      error => {
        this.loading.dismiss();
        this.api.showToast("Request failed, Team Profile not saved");
      }
    );
  }

  loadTeamCoaches() {
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    this.api
      .getRequset(this.url.assistantCoach + this.globVar.team.id)
      .subscribe(
        response_api => {
          this.loading.dismiss();
          try {
            this.assistantCoach= [];
            this.assistantCoach = response_api.data.assistantList;
          } catch (Error) {
            //this.api.showToast("No Assistant Coaches");
          }
        },
        error => {
          this.loading.dismiss();
          this.api.showToast("Request failed");
        }
      );
  }

  addCoach() {
    let addAssistantCoach = this.modalCtrl.create(AddCoach, {
      userId: 8675309
    });
    this.isAddCoachCalled = true;
    addAssistantCoach.onDidDismiss((data) => {
      if (this.isAddCoachCalled) {
        this.isAddCoachCalled = false;
        if(data)
        {this.loadTeamCoaches();}
      }
      // This will be executed after the modal is dismissed...
    });
    addAssistantCoach.present();
  }
}
