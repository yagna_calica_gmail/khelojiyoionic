import { AddExp } from './addExp/addExp';
import { User } from "../../model/user.model";
import { Observable } from "rxjs/Observable";
import { Component, ViewChild } from "@angular/core";
import { Http } from "@angular/http";
import {Content, NavController, LoadingController,Navbar, Events, ModalController } from "ionic-angular";
import { GooglePlus } from "@ionic-native/google-plus";
import {
  Url,
  DataServiceProvider
} from "../../providers/data-service/data-service";
import * as Constants from "../../model/constant.model";
import { AppPreferences } from '@ionic-native/app-preferences';
import { coachProfile } from "../../model/coachProfile.model";
import { Storage } from "@ionic/storage";
import { DatePicker } from "@ionic-native/date-picker/ngx";
import { Experiences } from '../../model/experiences.model';


@Component({
  selector: "page-userprofile",
  templateUrl: "userprofile.html"
})
export class UserProfile {
  @ViewChild('navbar') navBar: Navbar;
  @ViewChild(Content) content: Content;

  public sports: any[] = Constants.sports;

  public username: string;
  public password: string;
  public error: string;
  apiUrl = "http://72.249.170.12/BluetoothApi/api/Login/Login";
  passwordType: string = "password";
  paIcon: string = "eye-off";
  user: Observable<User[]>;
  loginError: string;
  uber: Observable<any>;
  userImage ="assets/imgs/avatar.jpeg";
  teamImage ="assets/imgs/team.png";
  profile: coachProfile
  Firstname = (this.globVar.user)?this.globVar.user.firstName:"N/A";;
  Lastname = (this.globVar.user)?this.globVar.user.lastName:"N/A";
  Email = (this.globVar.user)?this.globVar.user.email:"N/A";
  Phone =  (this.globVar.user)?this.globVar.user.phoneNumber:"N/A";
  currentPostion = "Coach";
  intitute  =  (this.profile)?this.profile.instituteName:"N/A";
  instituteAddress = (this.profile)?this.profile.instituteAddress:"N/A";
  coachingPhilosophy = (this.profile)?this.profile.coachingPhilosophy:"N/A";
  country= (this.profile)?this.profile.country:"N/A";
  state= (this.profile)?this.profile.state:"N/A";
  gender= (this.profile)?this.profile.gender:"N/A";
  dob= (this.profile)?this.profile.dob:"N/A";
  city= (this.profile)?this.profile.city:"N/A";
  pinCode= (this.profile)?this.profile.pinCode:"N/A";
  experience = (this.profile)?this.profile.experience:"N/A";
  achievements = (this.profile)?this.profile.achievements:"N/A";
  addExpCalled= false;
  experiences : Array <Experiences>= [];

  isEdit:boolean = false;

  /**
   * @name items
   * @type {any}
   * @public
   * @description     Defines an object for storing returned comics data
   */
  public items : any;

  /**
   * @name config
   * @type {any}
   * @public
   * @description     Defines an object allowing the interface properties to be accessed
   */


  public loading;
  constructor(
    private navCtrl: NavController,
    public http: Http,
    public gPlus: GooglePlus,
    public api: DataServiceProvider,
    public url: Url,
    public globVar: Url,
    public loadingCtrl: LoadingController,
    public prefs: AppPreferences,
    public publishEvent: Events,
    private storage: Storage,
    private datePicker: DatePicker,
    public modalCtrl: ModalController,


  ) {
    //this.user = this.firebaseProvider.getShoppingItems();
  }
  //https://www.djamware.com/post/59924f9080aca768e4d2b12e/ionic-3-consuming-rest-api-using-new-angular-43-httpclient

  ionViewDidLoad(): void {
    this.getUserProfile();
  }


  editView(status:boolean){
    this.isEdit=status;
  }

  ionViewDidEnter() {
    this.navBar.backButtonClick = () => {
      ///here you can do wathever you want to replace the backbutton event
      if(this.isEdit === true)
      {
        this.editView(false);
      }
      else{
        this.discard();
      }
    };
  }

  discard(){
    this.navCtrl.pop();
  }

  save(){
    this.loading = this.loadingCtrl.create({ content: "please wait..." });
    this.loading.present();
    let obj = { FirstName : this.Firstname ,
                LastName: this.Lastname ,
                Email: this.Email.toLowerCase() ,
                Id : this.globVar.user.id,
                PhoneNumber: this.Phone
              };

    this.api.postRequset(this.url.updateProfile ,obj).subscribe( (response_api) =>{
      this.loading.dismiss();
      if(response_api.status == Constants.OK)
       {
            this.savePersonalInfo();
       }
       else{
         this.api.showToast("Failed: "+response_api.errors[0].code);
       }
       },
         error => {
        this.loading.dismiss();
        this.api.showToast("Error: "+error.message+obj.Id) ;
    });
  }

  savePersonalInfo(){
    this.loading = this.loadingCtrl.create({ content: "please wait..." });

    this.loading.present();
    let obj =     {
      AspnetuserId:this.globVar.user.id,
      Gender: this.gender,
      Dob:this.dob,
      ContactNo:this.Phone,
      Country:this.country,
      State:this.state,
      City:this.city,
      PinCode:this.pinCode,
      InstituteName:this.intitute,
      InstituteAddress:this.instituteAddress,
      Experience:JSON.stringify(this.experiences),
      Achievements:this.achievements,
      CoachingPhilosophy:this.coachingPhilosophy
}

    this.api.postRequset(this.url.updateCoachProfile ,obj).subscribe( (response_api) =>{
      this.loading.dismiss();
      if(response_api.status == Constants.OK)
       {
        this.getUser();
       }
       else{
         this.api.showToast("Failed: ");
       }
       },
         (error) => {
        this.loading.dismiss();
        this.api.showToast("Error: "+error.message) ;
    });
  }

  getUserProfile(){
  //  this.loading = this.loadingCtrl.create({ content: "please wait..." });

  //  this.loading.present();

      this.api.getRequset(this.url.getCoachProfileById + this.globVar.user.id).subscribe( (response_api) =>{
      if(response_api.status == Constants.OK)
       {

        this.profile = response_api.coachProfile;
        this.currentPostion =  "Coach";
        this.intitute  =  (this.profile)?this.profile.instituteName:"N/A";
        this.instituteAddress = (this.profile)?this.profile.instituteAddress:"N/A";
        this.coachingPhilosophy = (this.profile)?this.profile.coachingPhilosophy:"N/A";
        this.country= (this.profile)?this.profile.country:"N/A";
        this.state= (this.profile)?this.profile.state:"N/A";
        this.gender= (this.profile)?this.profile.gender:"";
        this.dob= (this.profile)?this.profile.dob:"";
        this.city= (this.profile)?this.profile.city:"N/A";
        this.pinCode= (this.profile)?this.profile.pinCode:"";
        this.experiences = (this.profile.experience)?JSON.parse(this.profile.experience):[];
        this.achievements = (this.profile)?this.profile.achievements:"N/A";

       }
       else{
        this.api.showToast("Failed: "+response_api.errors[0].code);
       }
       },
        error => {
        this.api.showToast("error"+error.message) ;
    });

  }
  getUser(){
    //  this.loading = this.loadingCtrl.create({ content: "please wait..." });
    //  this.loading.present();
        this.api.getRequset(this.url.getUserbyId + this.globVar.user.id).subscribe( (response_api) =>{
    //    this.loading.dismiss();
        if(response_api.status == Constants.OK)
         {
          this.globVar.user = response_api.users;
          this.storage.set ( 'user',  this.globVar.user);
          this.publishEvent.publish("userProfile",{action:"loadUserProfile"} );
          this.editView(false);
         }
         else{
          this.api.showToast("Failed: "+response_api.errors[0].code);
         }
         },
          error => {
          this.api.showToast("error"+error.message) ;
      });
    }

    datePickerfunc() {
      this.datePicker
        .show({
          date: new Date(),
          mode: "date",
          androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        })
        .then(
          date => console.log("Got date: ", date),
          err => console.log("Error occurred while getting date: ", err)
        );
    }

    addExperience() {
      let addExpModal = this.modalCtrl.create(AddExp, { data: null });
      this.addExpCalled=true;
      addExpModal.onDidDismiss((data) => {
        if (this.addExpCalled) {
          this.addExpCalled = false;
          if (data) {
          this.experiences[this.experiences.length]=data;
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addExpModal.present();
    }

    editExp(index:any){
      let addExpModal = this.modalCtrl.create(AddExp, { data: this.experiences[index] });
      this.addExpCalled=true;
      addExpModal.onDidDismiss((data) => {
        if (this.addExpCalled) {
          this.addExpCalled = false;
          if (data) {
          this.experiences[index]=data;
          }
        }
        // This will be executed after the modal is dismissed...
      });
      addExpModal.present();
    }
}
