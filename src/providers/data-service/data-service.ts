import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import "rxjs/add/operator/map";
import { User } from "../../model/user.model";
import { Team } from "../../model/team.model.";
import { ToastController } from "ionic-angular";

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Url {
  public editPlayerPersonal = true;
  public editPlayerAcadmics = true;
  public editPlayerAthletics = true;

  public ok: string = "200";
  public userImage ="assets/imgs/avatar.jpeg";
  public selectedTeamIndex: number = 0;
  public teams: Array<Team> = [];
  public team: Team = this.teams[this.selectedTeamIndex];
  public selectedTeam : Team = this.teams[this.selectedTeamIndex];
  public user: User;
  public firstName = (this.user)?this.user.firstName:"";;
  public lastName = (this.user)?this.user.lastName:"";
  public email = (this.user)?this.user.email:"";
  appSubcribeflag:boolean=false;
  rosterSubcribeflag:boolean=false;
  teamProfSubcribeflag:boolean=false;
  teamtabSubcribeflag:boolean=false;
  scheduleSubcribeflag:boolean=false;
  isPlayer:boolean=true;
  public token: string = "";

  //public host = "http://192.168.10.135";
  public host = "http://3.18.107.149:8888/";
  public baseUrl = this.host + "/api/";
  public login = this.baseUrl + "account/login";
  public socailLogin = this.baseUrl + "account/checkAuthenticUser";
  public register = this.baseUrl + "account/register";
  public socialRegistration = this.baseUrl + "account/socialAuthenticUser";
  public updateProfile = this.baseUrl + "account/updateUser";
  public getUserbyId = this.baseUrl + "account/getUserById?id=";
  public getTeamsbyId = this.baseUrl + "team/getTeamById?id=";
  public addTeam = this.baseUrl + "team/addTeam";
  public createEvent = this.baseUrl + "event/createEvents?teamId=";
  public getEvents = this.baseUrl + "event/getEventsList?coachId=";
  public getScheduleEvent = this.baseUrl + "event/getScheduleEvent?coachId=";
  public updateTeam = this.baseUrl + "team/updateTeam";
  public getPlayerList = this.baseUrl + "event/getPlayerList?searchString=";
  public getAssistantCoachList = this.baseUrl + "event/getAssistantCoachList?searchString=";
  public addAssitantCoach = this.baseUrl + "team/addAssistantCoach";
  public addAthlete = this.baseUrl + "team/addAthlete";
  public addRaceScore = this.baseUrl + "TrackAndField/addRaceScore"
  public getRaceScore = this.baseUrl + "TrackAndField/getScoreRace?eventId=";
  public getTeamRosters(team: String, userid: String) {
    return (
      this.baseUrl + "team/Athletes?userTeam=" + team + "&userid=" + userid
    );
  }
  public assistantCoach= this.baseUrl+"team/AssistantCoach?userTeam=";
  public getVenueList= this.baseUrl+"Event/GetVenueList?searchString=";
  public getCoachProfileById= this.baseUrl+ "Account/GetCoachProfileById?id="
  public updateCoachProfile = this.baseUrl+ "Account/updateCoachProfile";

  public getPlayerProfileById=this.baseUrl+"Account/getPlayerProfileById?id="
  public updatePlayerProfile=this.baseUrl+"Account/updatePlayerProfile"
  constructor() {}
  public GetPlayerAcademicProfileById=this.baseUrl+"Account/GetPlayerAcademicProfileById?id="
  public getPlayer100meterStatistics = this.baseUrl+ "TrackAndField/getPlayer100meterStatistics?Id="
  public addPlayer100meterStatistics = this.baseUrl+ "TrackAndField/addPlayer100meterStatistics"

}

@Injectable()
export class DataServiceProvider {
  constructor(public http: HttpClient,private toastCtrl: ToastController) {
    console.log("Hello DataServiceProvider Provider");
  }

  public showToast(msg : string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    // toast.onDidDismiss(() => {
    //   //console.log('Dismissed toast');
    // });

    toast.present();
  }


  postRequset(url: string, param: any): any {

    return this.http.post(url, param, {
      headers: { "Content-Type": "application/json" }
    });


  }

  putRequset(url: string, param: any): any {
    return this.http.put(url, param, {
      headers: { "Content-Type": "application/json" }
    });
  }

  getRequset(url: string): any {
    return this.http.get(url, {
      headers: { "Content-Type": "application/json" }
    });
  }


}
